// https://html.spec.whatwg.org/multipage/webappapis.html#windowtimers
var WindowTimers = {
  setTimeout: setTimeout,
  clearTimeout: clearTimeout,
  setInterval: setInterval,
  clearInterval: clearInterval
};

export default WindowTimers;
