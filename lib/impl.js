import * as events from './events.js';
import {elements as htmlElements} from './htmlelts.js';
import {elements as svgElements} from './svg.js';

import CSSStyleDeclaration from './CSSStyleDeclaration.js';
import CharacterData from './CharacterData.js';
import Comment from './Comment.js';
import DOMException from './DOMException.js';
import DOMImplementation from './DOMImplementation.js';
import DOMTokenList from './DOMTokenList.js';
import Document from './Document.js';
import DocumentFragment from './DocumentFragment.js';
import DocumentType from './DocumentType.js';
import Element from './Element.js';
import HTMLParser from './HTMLParser.js';
import NamedNodeMap from './NamedNodeMap.js';
import Node from './Node.js';
import NodeList from './NodeList.js';
import NodeFilter from './NodeFilter.js';
import ProcessingInstruction from './ProcessingInstruction.js';
import Text from './Text.js';
import Window from './Window.js';

export default {
  CSSStyleDeclaration,
  CharacterData,
  Comment,
  DOMException,
  DOMImplementation,
  DOMTokenList,
  Document,
  DocumentFragment,
  DocumentType,
  Element,
  HTMLParser,
  NamedNodeMap,
  Node,
  NodeList,
  NodeFilter,
  ProcessingInstruction,
  Text,
  Window,
  ...events,
  ...htmlElements,
  ...svgElements,
}
