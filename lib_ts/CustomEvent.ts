export default CustomEvent;

import Event from './Event.ts';

function CustomEvent(type, dictionary) {
  // Just use the superclass constructor to initialize
  Event.call(this, type, dictionary);
}
CustomEvent.prototype = Object.create(Event.prototype, {
  constructor: { value: CustomEvent }
});
