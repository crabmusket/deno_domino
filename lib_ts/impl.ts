import * as events from './events.ts';
import {elements as htmlElements} from './htmlelts.ts';
import {elements as svgElements} from './svg.ts';

import CSSStyleDeclaration from './CSSStyleDeclaration.ts';
import CharacterData from './CharacterData.ts';
import Comment from './Comment.ts';
import DOMException from './DOMException.ts';
import DOMImplementation from './DOMImplementation.ts';
import DOMTokenList from './DOMTokenList.ts';
import Document from './Document.ts';
import DocumentFragment from './DocumentFragment.ts';
import DocumentType from './DocumentType.ts';
import Element from './Element.ts';
import HTMLParser from './HTMLParser.ts';
import NamedNodeMap from './NamedNodeMap.ts';
import Node from './Node.ts';
import NodeList from './NodeList.ts';
import NodeFilter from './NodeFilter.ts';
import ProcessingInstruction from './ProcessingInstruction.ts';
import Text from './Text.ts';
import Window from './Window.ts';

export default {
  CSSStyleDeclaration,
  CharacterData,
  Comment,
  DOMException,
  DOMImplementation,
  DOMTokenList,
  Document,
  DocumentFragment,
  DocumentType,
  Element,
  HTMLParser,
  NamedNodeMap,
  Node,
  NodeList,
  NodeFilter,
  ProcessingInstruction,
  Text,
  Window,
  ...events,
  ...htmlElements,
  ...svgElements,
}
