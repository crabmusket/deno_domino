/* Domino uses sloppy-mode features (in particular, `with`) for a few
 * minor things.  This file encapsulates all the sloppiness; every
 * other module should be strict. */
/* jshint strict: false */
/* jshint evil: true */
/* jshint -W085 */
export default {
  Window_run: function _run(code, file) {
    throw new Error('sloppy Window_run not implemented');
  },
  EventHandlerBuilder_build: function build() {
    throw new Error('sloppy EventHandlerBuilder_build not implemented');
  }
};
