# deno_domino

This is a port of https://github.com/fgnass/domino for use in https://deno.land

The intent is to maintain an identical API; fixes will be related to deno's import conventions, and adding types for compatibility with TypeScript.
